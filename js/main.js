// Чекбоксы

$("input[type='radio']").ionCheckRadio();
$("input[type='checkbox']").ionCheckRadio();


$(".btn-modal").fancybox({
    'padding'    : 0,
    'closeBtn'   : false,
    'tpl'        : {
        wrap     : '<div class="fancybox-wrap" tabIndex="-1"><div class="fancybox-skin modal-video"><div class="fancybox-outer"><div class="fancybox-inner fancybox-scroll"></div></div></div></div>'
    }
});